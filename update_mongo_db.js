"use strict";

const cheerio = require('cheerio');
const query = require('array-query');
const MongoClient = require('mongodb').MongoClient;

var url = 'mongodb://localhost:27017/carpark_status_hackathon';

MongoClient.connect(url, function(err, db) {

    db.collection('parkhouses').find().toArray().then(((parkhouses_static) => {
        console.log("Founded Parkhouses");
        db.collection('carparks_status').find().toArray().then((timestamps) => {
            console.log("Founded carparks_status");
            timestamps.forEach((timestamp, i) => {
                timestamp.carparks.forEach((item) => {
                    let static_parkhouse = query('name').is(item.label).on(parkhouses_static).pop();
                    if (static_parkhouse) {
                        item.free = item.count;
                        item.location = static_parkhouse.location;
                        item.total = static_parkhouse.total;
                    }
                });
                db.collection('carparks_timestamps').insertOne({
                    timestamp: timestamp.timestamp,
                    carparks: timestamp.carparks
                }).then(() => {
                    console.log("Inserted: " + i);
                });


            });
            console.log("Close DB");

        })
    }));

});
