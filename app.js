"use strict";

const request = require('request');
const cheerio = require('cheerio');
const query = require('array-query');
const MongoClient = require('mongodb').MongoClient;

var url = 'mongodb://localhost:27017/carpark_status_hackathon';

MongoClient.connect(url, function(err, db) {
  console.log("Connected correctly to server.");

  request('http://www.parken-mannheim.de/', function (error, response, html) {
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(html);

      let carparks = [];

      db.collection('parkhouses').find().toArray().then((parkhouses_static) => {
        /**
         * Load carpark list
         */
        $('#pls-labels > a').each((i, item) => {
          let $item = $(item);
          carparks.push({
            label: $item.text().trim(),
            title: $item.attr('title')
          });
        });

        /**
         * Load carpark free count
         */
        let freeParkingSpaces = $('#pls-liste')
            .text()
            .split('\n')
            .map((data) => data.trim())
            .filter((data) => data != '');

        carparks.forEach((item, i) => {
          item.free = parseInt(freeParkingSpaces[i]);
          let static_parkhouse = query('name').is(item.label).on(parkhouses_static).pop();
          if (static_parkhouse) {
            item.location = static_parkhouse.location;
            item.total = static_parkhouse.total;
          }
        });

        console.log('Got car park status, saving', carparks);


        db.collection('carparks_timestamps').insertOne({
          timestamp: new Date(),
          carparks: carparks
        });


        db.close();
      });

    }
  });
});
